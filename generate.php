<?php

class Helper
{
	public function loadFile($target)
	{
		$this->data = file_get_contents($target);
	}
}

class CssParser extends Helper
{
	public $data;
	private $out = array();


	private function lineUpCss()
	{
		$this->data = preg_replace("/(\r\n|\r|\n)/", "", $this->data);
		$this->data = preg_replace("/([\s])/", "", $this->data);
		$this->data = preg_replace("/}./", "}\n.", $this->data);
	}

	public function parseData()
	{
		$this->lineUpCss();
		$tmp = explode("\n", $this->data);
		if (count($tmp) > 0) {
			foreach ($tmp as $t) {
				preg_match_all('/^\.([a-zA-Z0-9\-_]+)\{([a-zA-Z0-9#\:\;\-_]+)\}$/', $t, $_tmp);
				if (count($_tmp[0]) > 0) {
					$this->out[$_tmp[1][0]] = $_tmp[2][0];
				}
			}
		}
	}

	public function getData()
	{
		return $this->out;
	}
}

class HtmlParser extends Helper
{
	public $data;
	public $css = array();

	public function getData()
	{
		return $this->data;
	}

	public function loadCss(Array $css = array())
	{
		$this->css = $css;
		$this->data = preg_replace('/\<link(.*)\/\>/', '', $this->data);
		if (count($this->css) > 0) {
			foreach ($this->css as $k=>$v) {
				$this->data = str_replace('class="'.$k.'"', 'style="'.$v.'"', $this->data);
			}
		}
	}
}

$css = new CssParser();
$cssLocation = dirname(__FILE__).'/css/style.css';
$css->loadFile($cssLocation);
$css->parseData();
//$css->getData();

$ht = new HtmlParser();
$htLocation = dirname(__FILE__).'/index.html';
$ht->loadFile($htLocation);
$ht->loadCss($css->getData());
echo $ht->getData();